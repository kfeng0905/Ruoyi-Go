package main

import (
	"context"
	"flag"
	"fmt"
	"github.com/gin-middleware/xxl-job-executor"
	"log"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"ruoyi-go/config"
	"ruoyi-go/pkg/logs"
	"ruoyi-go/pkg/scheduler"
	"ruoyi-go/routers"
	"ruoyi-go/utils"
	"strconv"
	"time"
)

var configFile = flag.String("f", "./config.yaml", "")

// @title Ruoyi-Go 接口文档
// @version v1.0.0
// @description 基于Go，gin，JWT，vue前后端分离的权限管理系统
// @contact.name Ruoyi-Go
// @contact.url https://gitee.com/OptimisticDevelopers/Ruoyi-Go
// @host 127.0.0.1:8080
// @BasePath /
func main() {
	fmt.Println("hello ruoyi go")
	logs.Poster()
	// 初始化配置文件
	config.InitAppConfig(*configFile)
	// 初始化 定时
	scheduler.InitCron()
	// xxl_job
	cron := scheduler.InitXxlJobCron()

	// 初始化路由
	r := routers.Init()

	if config.XxlJob.Enabled {
		xxl_job_executor_gin.XxlJobMux(r, cron)
	}

	//打开浏览器
	if runtime.GOOS == "windows" {
		utils.OpenWin("http://127.0.0.1:" + strconv.Itoa(config.Server.Port))
	}

	if runtime.GOOS == "darwin" {
		utils.OpenMac("http://127.0.0.1:" + strconv.Itoa(config.Server.Port))
	}

	//if err := r.Run(":" + strconv.Itoa(config.Server.Port)); err != nil {
	//	fmt.Printf("startup service failed, err:%v\n\n", err)
	//}
	srv := &http.Server{
		Addr:    ":" + strconv.Itoa(config.Server.Port),
		Handler: r,
	}

	go func() {
		// 服务连接
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// 等待中断信号以优雅地关闭服务器（设置 5 秒的超时时间）
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}
	log.Println("Server exiting")
}
