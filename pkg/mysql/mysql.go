package mysql

import (
	"log"
	"os"
	"ruoyi-go/config"
	"sync"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var once sync.Once

// 定义数据库连接结构体
type connect struct {
	primaryDB *gorm.DB
	//secondaryDB *gorm.DB
	currentDB *gorm.DB
}

// 设置一个全局变量保存数据库连接
var _connect *connect

// 连接 MySQL 数据库
func connectMysql() {
	// 启用打印日志
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold: time.Second, // 慢 SQL 阈值
			LogLevel:      logger.Info, // 日志等级: Silent、Error、Warn、Info
			Colorful:      false,       // 禁用彩色打印
		},
	)

	// 从配置文件获取主库的 DSN
	primary := config.Database.Primary.UserName + ":" + config.Database.Primary.Password + "@tcp(" + config.Database.Primary.Host + ":" + config.Database.Primary.Port + ")/" + config.Database.Primary.DbName + "?charset=utf8mb4&parseTime=True&loc=Local"

	/*	// 从配置文件获取从库的 DSN
		secondary := config.Database.Secondary.UserName + ":" + config.Database.Secondary.Password + "@tcp(" + config.Database.Secondary.Host + ":" + config.Database.Secondary.Port + ")/" + config.Database.Secondary.DbName + "?charset=utf8mb4&parseTime=True&loc=Local"
	*/
	// 打开主库连接
	primaryDB, err := gorm.Open(mysql.Open(primary), &gorm.Config{
		Logger: newLogger,
	})
	if err != nil {
		log.Printf("主数据库连接失败:", err)
		return
	}

	/*// 打开从库连接
	secondaryDB, err := gorm.Open(mysql.Open(secondary), &gorm.Config{
		Logger: newLogger,
	})
	if err != nil {
		log.Println("从数据库连接失败:", err)
		return
	}*/

	// 初始化数据库连接结构体，默认使用主数据库
	_connect = &connect{
		primaryDB: primaryDB,
		//secondaryDB: secondaryDB,
		currentDB: primaryDB, // 默认连接到主数据库
	}
}

// SetPrimary 切换到主库
func SetPrimary() {
	if _connect != nil {
		_connect.currentDB = _connect.primaryDB
	}
}

/*// SetSecondary 切换到从库
func SetSecondary() {
	if _connect != nil {
		_connect.currentDB = _connect.secondaryDB
	}
}*/

/*
 默认使用主库
db := mysql.MysqlDb()
 进行数据库操作
var result interface{}
db.First(&result)
fmt.Println(result)

 切换到从库
mysql.SetSecondary()
db = mysql.MysqlDb()
 进行数据库操作
db.First(&result)
fmt.Println(result)

 切换回主库
mysql.SetPrimary()
db = mysql.MysqlDb()
 进行数据库操作
db.First(&result)
fmt.Println(result)*/

// MysqlDb 获取当前使用的数据库连接
func MysqlDb() *gorm.DB {
	if _connect == nil {
		once.Do(func() {
			connectMysql()
		})
	}
	return _connect.currentDB
}
